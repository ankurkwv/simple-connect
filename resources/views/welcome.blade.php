<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  
  <title>SimpleConnect.io</title>
  
  <meta name="robots" content="noindex">

  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="/css/app-compiled.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
  <link rel="icon" type="image/png" href="images/ankur.png" />

</head>

<body>

  <div class="container">

    <h1>Simple Connect</h1>

    <p>Yesterday at Laracon, I really wish I had a way to simply connect with those I was meeting.</p>

    <p class="big">I introduce, Simple Connect.</p>
    <ul>
      <li>Text "START" to the bot. <a href="sms:+14352222215">US Number</a> | <a href="sms:+32460231856">EU Number</a></li>
      <li>Fill out your name, email, and a greeting.</li>
      <li>Now, just ask anyone you meet to put their email into your bot, and Simple Connect will send them a connection email on your behalf! 🎉</li>
    </ul>
  </div>
</body>