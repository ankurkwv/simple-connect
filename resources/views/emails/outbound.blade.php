@component('mail::message')
# New Connection

{{ $body }}

Powered by [simpleconnect.io](https://simpleconnect.io)
@endcomponent
