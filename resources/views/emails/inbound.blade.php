@component('mail::message')
# New Connection
{{ $connectionEmail }}

# Your Notes
{{ $notes }}

Powered by [simpleconnect.io](https://simpleconnect.io)
@endcomponent
