<?php

namespace App\Http\Controllers;

use App\Connector;
use App\Mail\OutboundConnect;
use App\Mail\InboundConnect;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use Aloha\Twilio\Twilio;
use Valitron\Validator;

class MessageController extends Controller
{
	private $data;
	private $twilio;
	private $connector;

    /**
     * Create a new MessageController instance.
     *
     * @return void
     */
    public function __construct()
    {
    	$this->middleware('twilio')->only('index');
    }

    /**
     * Correctly route the incoming message.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	$this->data = $this->getMessageData($request);
    	$this->twilio = new Twilio(
    		env('TWILIO_SID'), 
    		env('TWILIO_TOKEN'), 
    		$this->data['to']
    	);

    	$this->connector = Connector::where('user_phone', $this->data['from'])
    				->where('service_phone', $this->data['to'])
    				->first();

		if ($this->connector && $this->connector->conversation_status) {
			$methodName = "handle" . ucwords($this->connector->conversation_status, "_");
			$methodName = str_replace("_", "", $methodName);
			if ( ! method_exists($this, $methodName)) {
				\Log::error('Method not available: ' . $methodName);
				return $this->sendMessagesToUser('Woops! Seems we broke this... try again?');
			}
			return $this->$methodName();
		}

    	return $this->handleFirstText();
    }

    private function handleFirstText() {
    	$this->createConnector();
    	return $this->sendMessagesToUser('Welcome to Simple Connect. We are here to help you make your connections last. What is your full name?');
    }

    private function handleAskName() {
    	$text = filter_var($this->data['text'], FILTER_SANITIZE_STRING);
    	$this->connector->user_name = $text;
    	$this->connector->conversation_status = 'ask_email';
    	$this->connector->save();
    	return $this->sendMessagesToUser('Great! What is your email?');
    }

    private function handleAskEmail() {
    	if ( ! $this->validateMessageText('emailDNS')) {
    		return $this->sendMessagesToUser('That email doesn\'t look quite right. Try again.');
    	}
    	$this->connector->user_email = $this->data['text'];
    	$this->connector->conversation_status = 'ask_message';
    	$this->connector->save();
    	return $this->sendMessagesToUser([
    		'Finally, what will your greeting say? Here is an example:', 
    		'Hi there! Ankur here from Laracon EU. It was great meeting you, and I hope to stay in touch! You have my email now and I am @ankurkwv on Instagram :)'
    	]);
    }

    private function handleAskMessage() {
    	$text = filter_var($this->data['text'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
    	$this->connector->user_message = $text;
    	$this->connector->conversation_status = 'ask_contact';
    	$this->connector->save();
    	return $this->sendMessagesToUser('Ready for a new connection! Just enter their email to start!');
    }

    private function handleAskContact() {
    	if ( ! $this->validateMessageText('emailDNS')) {
    		return $this->sendMessagesToUser('Their email doesn\'t look quite right. Try again.');
    	}
    	$this->connector->latest_contact = $this->data['text'];
    	$this->connector->conversation_status = 'ask_note';
    	$this->connector->save();
		Mail::queue(new OutboundConnect($this->data['text'], $this->connector));
    	return $this->sendMessagesToUser('We sent them your contact & message. Reply now with any notes you have about them, and we will email them your way.');
    }

    private function handleAskNote() {
    	$text = filter_var($this->data['text'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
    	$this->connector->conversation_status = 'ask_contact';
    	$this->connector->save();
		Mail::queue(new InboundConnect($this->connector, $text));
    	return $this->sendMessagesToUser('Saved & ready for a new connection! Just enter their email to start!');
    }

    private function createConnector() {
    	$connector = new Connector;
    	$connector->user_phone = $this->data['from'];
    	$connector->service_phone = $this->data['to'];
    	$connector->conversation_status = 'ask_name';
    	$connector->save();
    }

    private function sendMessagesToUser($messages) {
		if (!is_array($messages)) {
			$messages = array($messages);
		}

		foreach ($messages as $message) {
    		$this->twilio->message($this->data['from'], $message);
		}

    	return $this->safeTwilioResponse();
    }

    private function validateMessageText($rule) {
    	$validator = new Validator($this->data);
    	$validator->rule($rule, 'text');
    	return $validator->validate();
    }

    private function safeTwilioResponse() {
    	return response("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<Response/>", 200, ['Content-Type' => 'text/xml']);
    }

    private function getMessageData($request)
    {
        $text = trim($request['Body']);

        return $data = [
            'from' => $request['From'],
            'to' => $request['To'],
            'text' => $text,
        ];
    }
}