<?php

namespace App\Http\Middleware;

use Closure;
use Twilio\Security\RequestValidator;

class ValidateTwilioRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $requestValidator = new RequestValidator(env('TWILIO_TOKEN'));

      $isValid = $requestValidator->validate(
        $request->header('X-Twilio-Signature'),
        $request->fullUrl(),
        $request->toArray()
      );

      if ($isValid || env('APP_DEBUG') === true) {
        return $next($request);
      } else {
        abort(403, 'You are not Twilio');
      }
    }
}
