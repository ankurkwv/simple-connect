<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Connector;

class OutboundConnect extends Mailable
{
    use Queueable, SerializesModels;
    
    public $connectionEmail;
    public $connector;
    public $body;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($connection_email, Connector $connector)
    {
        $this->connectionEmail = $connection_email;
        $this->connector = $connector;
        $this->body = $connector->user_message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.outbound')
                    ->subject('New Connection: ' . $this->connector->full_name)
                    ->to($this->connectionEmail)
                    ->replyTo($this->connector->user_email);
    }
}
