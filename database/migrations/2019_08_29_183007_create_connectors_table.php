<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConnectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('connectors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_phone');
            $table->string('service_phone');
            $table->string('user_name')->nullable();
            $table->string('user_email')->nullable();
            $table->string('latest_contact')->nullable();
            $table->text('user_message')->nullable();
            $table->string('conversation_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('connectors');
    }
}
