<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/twilio', 'MessageController@index');

Route::get('mailable', function () {
    $connector = App\Connector::first();

    return new App\Mail\InboundConnect('me@blah.com','blah','blslksdlsdks');
});